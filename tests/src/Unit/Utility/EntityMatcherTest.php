<?php

declare(strict_types=1);

namespace Drupal\Tests\jsld\Unit\Utility;

use Drupal\jsld\Utility\EntityMatcher;
use Drupal\Tests\UnitTestCase;

/**
 * Provides a test for entity matcher.
 *
 * @covers \Drupal\jsld\Utility\EntityMatcher
 * @group jsld
 */
final class EntityMatcherTest extends UnitTestCase {

  /**
   * Tests matcher.
   *
   * @dataProvider matchProvider
   */
  public function testMatch(string $bundle, string $view_mode, array $entity_limit, bool $expected_result): void {
    $result = EntityMatcher::matchBundleViewMode(
      bundle: $bundle,
      view_mode: $view_mode,
      limits: $entity_limit,
    );
    self::assertEquals($expected_result, $result);
  }

  /**
   * Provides data for testing matches.
   */
  public function matchProvider(): \Generator {
    yield 'missing separator' => [
      'bundle' => 'foo',
      'view_mode' => 'bar',
      'entity_limit' => ['foo'],
      'expected_result' => FALSE,
    ];

    yield 'empty entity limit' => [
      'bundle' => 'foo',
      'view_mode' => 'bar',
      'entity_limit' => [],
      'expected_result' => FALSE,
    ];

    yield 'no match' => [
      'bundle' => 'foo',
      'view_mode' => 'bar',
      'entity_limit' => ['bar|foo'],
      'expected_result' => FALSE,
    ];

    yield 'exact match' => [
      'bundle' => 'foo',
      'view_mode' => 'bar',
      'entity_limit' => ['foo|bar'],
      'expected_result' => TRUE,
    ];

    yield 'match any bundle and any view mode' => [
      'bundle' => 'foo',
      'view_mode' => 'bar',
      'entity_limit' => ['*|*'],
      'expected_result' => TRUE,
    ];
  }

}
