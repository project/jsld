<?php

declare(strict_types=1);

namespace Drupal\Tests\jsld\Unit\Hook\Entity;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\jsld\Hook\Entity\EntityView;
use Drupal\jsld\Plugin\jsld\JsldPluginInterface;
use Drupal\jsld\Plugin\jsld\JsldPluginManagerInterface;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;

/**
 * Tests the entity view hook handler.
 *
 * @covers \Drupal\jsld\Hook\Entity\EntityView
 * @group jsld
 */
final class EntityViewTest extends UnitTestCase {

  /**
   * Tests the hook handler.
   *
   * @dataProvider hookDataProvider
   */
  public function testHook(array $plugin_data, array $entity_data, string $view_mode, array $expected_build): void {
    $entity = $this->prepareEntity($entity_data);
    $display = $this->prophesize(EntityViewDisplayInterface::class);
    $display = $display->reveal();
    $plugin_manager = $this->preparePluginManager($plugin_data);
    $instance = new EntityView($plugin_manager);

    $build = [];
    $instance($build, $entity, $display, $view_mode);
    self::assertEquals($expected_build, $build);
  }

  /**
   * Prepares an entity mock.
   */
  private function prepareEntity(array $entity_data): EntityInterface {
    $entity = $this->prophesize(EntityInterface::class);
    $entity->getEntityTypeId()->willReturn($entity_data['entity_type_id']);
    $entity->bundle()->willReturn($entity_data['bundle']);

    return $entity->reveal();
  }

  /**
   * Prepares a plugin manager.
   */
  private function preparePluginManager(array $plugin_data): JsldPluginManagerInterface {
    $plugin = $this->prophesize(JsldPluginInterface::class);
    $plugin->isEnabled()->willReturn($plugin_data['is_enabled']);
    $plugin->build()->willReturn($plugin_data['build_result']);
    $plugin = $plugin->reveal();

    $plugin_manager = $this->prophesize(JsldPluginManagerInterface::class);
    $plugin_manager->getDefinitions()->willReturn([
      $plugin_data['id'] => [
        'entity_type' => $plugin_data['entity_type'],
        'entity_limit' => $plugin_data['entity_limit'],
      ],
    ]);
    $plugin_manager
      ->createInstance($plugin_data['id'], Argument::any())
      ->willReturn($plugin);

    return $plugin_manager->reveal();
  }

  /**
   * Provides data for hook testing.
   */
  public function hookDataProvider(): \Generator {
    yield 'valid plugin; unmatched entity' => [
      'plugin_data' => [
        'id' => 'foo',
        'entity_type' => 'foo',
        'entity_limit' => [
          'foo|*',
        ],
        'is_enabled' => TRUE,
        'build_result' => [],
      ],
      'entity_data' => [
        'entity_type_id' => 'bar',
        'bundle' => 'bar',
      ],
      'view_mode' => 'foo',
      'expected_build' => [],
    ];

    yield 'valid plugin; valid entity; plugin disabled' => [
      'plugin_data' => [
        'id' => 'foo',
        'entity_type' => 'foo',
        'entity_limit' => [
          'foo|*',
        ],
        'is_enabled' => FALSE,
        'build_result' => [],
      ],
      'entity_data' => [
        'entity_type_id' => 'foo',
        'bundle' => 'foo',
      ],
      'view_mode' => 'foo',
      'expected_build' => [],
    ];

    yield 'valid plugin; valid entity; plugin enabled' => [
      'plugin_data' => [
        'id' => 'foo',
        'entity_type' => 'foo',
        'entity_limit' => [
          'foo|*',
        ],
        'is_enabled' => TRUE,
        'build_result' => [
          '@context' => 'https://schema.org',
          '@type' => 'Article',
          'name' => 'foo',
        ],
      ],
      'entity_data' => [
        'entity_type_id' => 'foo',
        'bundle' => 'foo',
      ],
      'view_mode' => 'foo',
      'expected_build' => [
        '#attached' => [
          'html_head' => [
            0 => [
              [
                '#type' => 'html_tag',
                '#tag' => 'script',
                '#attributes' => [
                  'type' => 'application/ld+json',
                ],
                '#value' => '{"@context":"https:\/\/schema.org","@type":"Article","name":"foo"}',
              ],
              'jsld_foo',
            ],
          ],
        ],
      ],
    ];
  }

}
