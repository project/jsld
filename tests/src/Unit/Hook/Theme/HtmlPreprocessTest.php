<?php

declare(strict_types=1);

namespace Drupal\Tests\jsld\Unit\Hook\Theme;

use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\jsld\Data\PathMatchType;
use Drupal\jsld\Hook\Theme\HtmlPreprocess;
use Drupal\jsld\Plugin\jsld\JsldPluginInterface;
use Drupal\jsld\Plugin\jsld\JsldPluginManagerInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;

/**
 * Tests that JSON-LD added on a dedicated paths via hook_preprocess_html().
 *
 * @coversDefaultClass \Drupal\jsld\Hook\Theme\HtmlPreprocess
 * @group jsld
 */
final class HtmlPreprocessTest extends UnitTestCase {

  /**
   * Test the hook implementation.
   *
   * @dataProvider hookDataProvider
   */
  public function testHook(string $current_path, string $current_path_alias, array $plugin_data, bool $matches_path, array $expected_build): void {
    $plugin_manager = $this->preparePluginManager($plugin_data);
    $path_matcher = $this->preparePathMatcher(
      current_path: $current_path,
      current_path_alias: $current_path_alias,
      matches_path: $matches_path,
    );
    $path_current = $this->prepareCurrentPath($current_path);
    $alias_manager = $this->prepareAliasManager(
      current_path: $current_path,
      current_path_alias: $current_path_alias,
    );

    $variables = [];

    $instance = new HtmlPreprocess(
      entityPathPluginManager: $plugin_manager,
      pathMatcher: $path_matcher,
      currentPath: $path_current,
      aliasManager: $alias_manager,
    );
    $instance($variables);
    self::assertSame($expected_build, $variables);
  }

  /**
   * Builds a plugin manager.
   */
  private function preparePluginManager(array $plugin_data): JsldPluginManagerInterface {
    $plugin = $this->prophesize(JsldPluginInterface::class);
    $plugin->isEnabled()->willReturn($plugin_data['is_enabled']);
    $plugin->build()->willReturn($plugin_data['build_result']);
    $plugin = $plugin->reveal();

    $plugin_manager = $this->prophesize(JsldPluginManagerInterface::class);
    $plugin_manager->getDefinitions()->willReturn([
      $plugin_data['id'] => [
        'match_path' => $plugin_data['match_path'],
        'match_type' => $plugin_data['match_type'],
      ],
    ]);
    $plugin_manager
      ->createInstance($plugin_data['id'], Argument::any())
      ->willReturn($plugin);

    return $plugin_manager->reveal();
  }

  /**
   * Provides data for testing hook.
   */
  public function hookDataProvider(): \Generator {
    yield 'valid data for listed match' => [
      'current_path' => 'foo',
      'current_path_alias' => 'foo',
      'plugin_data' => [
        'id' => 'foo',
        'match_path' => ['foo'],
        'match_type' => PathMatchType::Listed,
        'is_enabled' => TRUE,
        'build_result' => [
          '@context' => 'https://schema.org',
          '@type' => 'Article',
          'name' => 'foo',
        ],
      ],
      'matches_path' => TRUE,
      'expected_build' => [
        '#attached' => [
          'html_head' => [
            0 => [
              [
                '#type' => 'html_tag',
                '#tag' => 'script',
                '#attributes' => [
                  'type' => 'application/ld+json',
                ],
                '#value' => '{"@context":"https:\/\/schema.org","@type":"Article","name":"foo"}',
              ],
              'jsld_foo',
            ],
          ],
        ],
      ],
    ];

    yield 'expected another path' => [
      'current_path' => 'foo',
      'current_path_alias' => 'foo',
      'plugin_data' => [
        'id' => 'foo',
        'match_path' => ['bar'],
        'match_type' => PathMatchType::Listed,
        'is_enabled' => TRUE,
        'build_result' => [],
      ],
      'matches_path' => FALSE,
      'expected_build' => [],
    ];

    yield 'valid data for unlisted match' => [
      'current_path' => 'foo',
      'current_path_alias' => 'foo',
      'plugin_data' => [
        'id' => 'foo',
        'match_path' => ['bar'],
        'match_type' => PathMatchType::Unlisted,
        'is_enabled' => TRUE,
        'build_result' => [
          '@context' => 'https://schema.org',
          '@type' => 'Article',
          'name' => 'foo',
        ],
      ],
      'matches_path' => FALSE,
      'expected_build' => [
        '#attached' => [
          'html_head' => [
            0 => [
              [
                '#type' => 'html_tag',
                '#tag' => 'script',
                '#attributes' => [
                  'type' => 'application/ld+json',
                ],
                '#value' => '{"@context":"https:\/\/schema.org","@type":"Article","name":"foo"}',
              ],
              'jsld_foo',
            ],
          ],
        ],
      ],
    ];

    yield 'same path but process only unlisted' => [
      'current_path' => 'foo',
      'current_path_alias' => 'foo',
      'plugin_data' => [
        'id' => 'foo',
        'match_path' => ['foo'],
        'match_type' => PathMatchType::Unlisted,
        'is_enabled' => TRUE,
        'build_result' => [],
      ],
      'matches_path' => TRUE,
      'expected_build' => [],
    ];

    yield 'disabled plugin, but the rest is valid' => [
      'current_path' => 'foo',
      'current_path_alias' => 'foo',
      'plugin_data' => [
        'id' => 'foo',
        'match_path' => ['foo'],
        'match_type' => PathMatchType::Listed,
        'is_enabled' => FALSE,
        'build_result' => [],
      ],
      'matches_path' => TRUE,
      'expected_build' => [],
    ];
  }

  /**
   * Prepares path matcher.
   */
  private function preparePathMatcher(string $current_path, string $current_path_alias, bool $matches_path): PathMatcherInterface {
    $path_matcher = $this->prophesize(PathMatcherInterface::class);
    $path_matcher
      ->matchPath(Argument::exact($current_path_alias), Argument::any())
      ->willReturn($matches_path)
      ->shouldBeCalled();

    $path_matcher
      ->matchPath(Argument::exact($current_path), Argument::any())
      ->willReturn($matches_path)
      ->shouldBeCalled();

    return $path_matcher->reveal();
  }

  /**
   * Prepares current path.
   */
  private function prepareCurrentPath(string $current_path): CurrentPathStack {
    $path_current = $this->prophesize(CurrentPathStack::class);
    $path_current->getPath()->willReturn($current_path);

    return $path_current->reveal();
  }

  /**
   * Prepares alias manager.
   */
  private function prepareAliasManager(string $current_path, string $current_path_alias): AliasManagerInterface {
    $alias_manager = $this->prophesize(AliasManagerInterface::class);
    $alias_manager
      ->getAliasByPath($current_path)
      ->willReturn($current_path_alias);

    return $alias_manager->reveal();
  }

}
