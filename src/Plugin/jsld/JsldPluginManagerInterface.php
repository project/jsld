<?php

declare(strict_types=1);

namespace Drupal\jsld\Plugin\jsld;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Defines an interface for plugin manager.
 */
interface JsldPluginManagerInterface extends PluginManagerInterface {

}
