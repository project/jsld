<?php

declare(strict_types=1);

namespace Drupal\jsld\Utility;

/**
 * Provides a helper for matching entities by view mode and bundles.
 */
final class EntityMatcher {

  /**
   * Matches given bundle and view mode for constraints.
   */
  public static function matchBundleViewMode(string $bundle, string $view_mode, array $limits = []): bool {
    foreach ($limits as $limit) {
      // Every limit must contain separator.
      if (!\str_contains($limit, '|')) {
        continue;
      }

      [$limit_bundle, $limit_view_mode] = \explode('|', $limit);

      $bundle_match = $bundle === $limit_bundle || $limit_bundle === '*';
      $view_mode_match = $view_mode === $limit_view_mode || $limit_view_mode === '*';

      if ($bundle_match && $view_mode_match) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
